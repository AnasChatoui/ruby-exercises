=begin
Convert the buttons entered on the microwave panel to their timer equivalent.
=end

class Microwave
    # initialization of parameter (seconds)
    def initialize(seconds)
      @seconds = seconds
    end

    def getTime
      if @seconds < 100
        hours = @seconds / 60
        minutes = @seconds % 60
      else
        hours = @seconds / 100
        minutes = @seconds % 100
      end
      [hours, minutes].map{|i| '%02d' % i}.join(':')
    end
end

# creation of an object initialize to (ex 90)
check = Microwave.new(90)
# retrieval of the value in hours and minutes format using the function (getTime)
puts check.getTime